package com.fb.CountriesProject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.CountriesProject.databinding.ActivityFirebase2Binding;
import com.example.CountriesProject.databinding.ActivityFirebaseBinding;
import com.example.CountriesProject.databinding.ActivityMainBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class FirebaseActivity extends AppCompatActivity {

    private ActivityFirebaseBinding binding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFirebaseBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        db = FirebaseFirestore.getInstance();
    }

    public void onClickCrearPais(View view) {
        Map<String, Object> pais = new HashMap<>();
        pais.put("codigo", 2);
        pais.put("nombre", "Benin");
        pais.put("capital", "Porto Novo");
        pais.put("poblacion", "11.49 millones");
        pais.put("bandera", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLLm4JbxT8o58svlyalDzKz0DPnxPO5tITeN9Dr1saBUnmOEmO&s");
        pais.put("himno", "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/gK8kIzQMNS8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>");

        db.collection("pais")
                .add(pais)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("FB", "Pais agregado correctamente: " + documentReference.getId());

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FB","Error pais no agregado",e);
                    }
                });
    }
}
