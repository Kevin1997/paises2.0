package com.example.CountriesProject;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import com.example.CountriesProject.adatador.AdapterPais;
import com.example.CountriesProject.databinding.ActivityMainBinding;
import com.example.CountriesProject.modelView.Pais;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{


    private ActivityMainBinding mainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = mainBinding.getRoot();
        setContentView(view);
        mainBinding.reciclerPais.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mainBinding.reciclerPais.setLayoutManager(layoutManager);
        RecyclerView.Adapter mAdapter = new AdapterPais(datos());
        mainBinding.reciclerPais.setAdapter(mAdapter);

    }


    public List<Pais>datos(){
        List<Pais> p= new ArrayList<>();
        p.add(new Pais("ANGOLA"," 30.81"," LUANDA",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLLm4JbxT8o58svlyalDzKz0DPnxPO5tITeN9Dr1saBUnmOEmO&s",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/gK8kIzQMNS8\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BENIN","11.49","porto novo",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Flag_of_Benin.svg/300px-Flag_of_Benin.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/JDzjadRDpNc\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BOTSUANA","2.25","Gaborone",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_Botswana.svg/1200px-Flag_of_Botswana.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/qm8aKzmCTVg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BURKINA","19.75","UAGADUGÚ",
                "https://www.geoenciclopedia.com/wp-content/uploads/2018/10/Afganistan-1.jpg",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/w4X-g_YVMYs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BURUNDI","11.18","BUYUMBURA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Flag_of_Burundi.svg/1200px-Flag_of_Burundi.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/2-PHkXde8WE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CABO VERDE","543.767","PRAIA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Flag_of_Cape_Verde.svg/300px-Flag_of_Cape_Verde.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/Qv3I4VoEw0k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CAMERÚN","25.22","YAUNDÉ",

                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Flag_of_Cameroon.svg/300px-Flag_of_Cameroon.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/E8yQDZ7jmNw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));





        p.add(new Pais("CHAD","15.48","YAMENA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Flag_of_Chad.svg/300px-Flag_of_Chad.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/n0-iPkXrJyI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("COMORAS","2.5","MORONI",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Flag_of_the_Comoros.svg/300px-Flag_of_the_Comoros.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6N2-uQrvNls\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("COSTA DE MARFIL","25.07","YAMUSUKRO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_C%C3%B4te_d%27Ivoire.svg/300px-Flag_of_C%C3%B4te_d%27Ivoire.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/rxrZhRD6G08\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("EGIPTO","98.42","EL CAIRO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_Egypt.svg/300px-Flag_of_Egypt.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/kipVmOylVyc\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ERITREA","2.5","ASMARA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Flag_of_Eritrea.svg/300px-Flag_of_Eritrea.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/8RQC04GQI-0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ETIOPÍA","2.56","ADÍS ABEBA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Flag_of_Ethiopia.svg/300px-Flag_of_Ethiopia.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/QZF0pk5wPdw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GABÓN","2.55","LIBREVILLE",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Flag_of_Gabon.svg/300px-Flag_of_Gabon.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/X0KaHfq5iBA\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GAMBIA","1.5","BANJUL",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_The_Gambia.svg/300px-Flag_of_The_Gambia.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/T74DiVRIEWg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GHANA","2.5","ACCRA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Ghana.svg/300px-Flag_of_Ghana.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/a738Q8N7Jxo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GUINEA","2.5","CONAKRY",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Flag_of_Guinea.svg/300px-Flag_of_Guinea.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GUINEA ECUATORIAL","2.5","MALABO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Flag_of_Equatorial_Guinea.svg/300px-Flag_of_Equatorial_Guinea.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/5ZlkOJV-miw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));


        p.add(new Pais("GUINEA-BISSAU","2.5","BISSAU",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Flag_of_Guinea-Bissau.svg/300px-Flag_of_Guinea-Bissau.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/zQMkCpciuMQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("KENIA","2.5","NAIROBI",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Kenya.svg/300px-Flag_of_Kenya.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("LESOTO","2.5","MASERU",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Flag_of_Lesotho.svg/300px-Flag_of_Lesotho.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("Ecuador","2.5","quito",
                "https://upload.wikimedia.org/wikipedia/commons/8/8f/Bandera_Del_Ecuador.jpg",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("LIBERIA","2.5","MONROVIA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Flag_of_Liberia.svg/300px-Flag_of_Liberia.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("LIBIA","2.5","TRÍPOLI",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Libya.svg/300px-Flag_of_Libya.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MADAGASCAR","2.5","ANTANANARIVO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Madagascar.svg/203px-Flag_of_Madagascar.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MALAUI","2.5","LILONGÜE",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Flag_of_Malawi.svg/203px-Flag_of_Malawi.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MALI","2.5","BAMAKO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Mali.svg/300px-Flag_of_Mali.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MARRUECOS","2.5","RABAT",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Morocco.svg/203px-Flag_of_Morocco.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MAURICIO","2.5","PORT LOUIS",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Flag_of_Mauritius.svg/203px-Flag_of_Mauritius.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MAURITANIA","2.5","NUAKCHOT",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Flag_of_Mauritania.svg/300px-Flag_of_Mauritania.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MOZAMBIQUE","2.5","MAPUTO",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Flag_of_Mozambique.svg/300px-Flag_of_Mozambique.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("NAMIBIA","2.5","WINDHOEK",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Flag_of_Namibia.svg/300px-Flag_of_Namibia.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("NÍGER","2.5","NIAMEY",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Flag_of_Niger.svg/193px-Flag_of_Niger.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("NIGERIA","2.5","ABUYA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Flag_of_Nigeria.svg/300px-Flag_of_Nigeria.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("REPÚBLICACENTROAFRICANA","2.5","BANGUI",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Central_African_Republic.svg/300px-Flag_of_the_Central_African_Republic.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("REPÚBLICA DEL CONGO","2.5","BRAZZAVILLE",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_the_Republic_of_the_Congo.svg/203px-Flag_of_the_Republic_of_the_Congo.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("REPÚBLICADEMOCRÁTICA DEL CONGO","2.5","KINSHASA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Flag_of_the_Democratic_Republic_of_the_Congo.svg/300px-Flag_of_the_Democratic_Republic_of_the_Congo.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("REPÚBLICA SAHARAUI","2.5","EL AAIÚN",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Flag_of_the_Sahrawi_Arab_Democratic_Republic.svg/203px-Flag_of_the_Sahrawi_Arab_Democratic_Republic.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("RUANDA","2.5","KIGALI",
                "https://www.banderas-mundo.es/data/flags/ultra/rw.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SANTO TOMÉ Y PRÍNCIPE","2.5","SANTO TOMÉ",
                "https://comprar-banderas.com/wp-content/uploads/2017/01/bandera-de-santo-tome-y-principe.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SENEGAL","2.5","DAKAR",
                "https://cdn.pixabay.com/photo/2017/08/19/04/07/international-2657347_1280.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SEYCHELLES","2.5","VICTORIA",
                "https://simbologiadelmundo.com/wp-content/uploads/2016/06/r-Seychelles.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));

        p.add(new Pais("SIERRA LEONA","2.5","FREETOWN",
                "https://es.historia.com/wp-content/uploads/2017/08/bandera-de-sierra-leona.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SOMALIA","2.5","MOGADISCIO",
                "https://www.banderas-mundo.es/data/flags/big/so.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SUAZILANDIA","2.5","MBABANE",
                "https://banderasmundo.es/banderas/suazilandia.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SUDÁFRICA","2.5","BLOEMFONTEIN",
                "https://cdn.pixabay.com/photo/2017/05/07/15/08/flag-2292685_960_720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SUDÁN DEL NORTE","2.5","JARTUM",
                "https://www.viajejet.com/wp-content/viajes/bandera-de-sudan.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SUDÁN DEL SUR","2.5","YUBA",
                "https://simbologiadelmundo.com/wp-content/uploads/2016/06/r-Sudan-del-Sur.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("TANZANIA","2.5","DODOMA",
                "https://www.banderas-mundo.es/data/flags/ultra/tz.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("TOGO","2.5","LOMÉ",
                "https://banderasmundo.es/banderas/togo.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("TÚNEZ","2.5","TÚNEZ",
                "https://i.pinimg.com/originals/c3/3c/99/c33c996db041cd0835ae3993e15a5a8e.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("UGANDA","2.5","KAMPALA",
                "https://i.pinimg.com/originals/86/6a/61/866a61eeb56c47cc4081868a598621ab.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("YIBUTI","2.5","YIBUTI",
                "https://www.banderas-mundo.es/data/flags/big/dj.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ZAMBIA","2.5","LUSAKA",
                "https://banderasmundo.es/banderas/zambia.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ZIMBABUE","2.5","HARARE",
                "https://cdn.pixabay.com/photo/2017/08/19/04/25/international-2657391_960_720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ANTIGUA Y BARBUDA","2.5","SAINT JOHN'S",
                "https://www.banderas-mundo.es/data/flags/ultra/ag.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ARGENTINA","2.5","BUENOS AIRES",
                "https://www.nicepng.com/png/detail/895-8953391_argentina-www-akukhanya-co-za-vibrant-colors-sol.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BAHAMAS","2.5","NASSAU",
                "https://www.lifeder.com/wp-content/uploads/2018/07/bahamas-162236_640-1.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BARBADOS","2.5","BRIDGETOWN",
                "https://www.banderas-mundo.es/data/flags/ultra/bb.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BELICE","2.5","BELMOPÁN",
                "https://www.lifeder.com/wp-content/uploads/2018/07/belice1-1280x720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BOLIVIA","2.5"," LA PAZ",
                "https://i.pinimg.com/originals/81/e2/02/81e2022a22be9999ea24e0c40f509bd9.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BRASIL","2.5","BRASILIA",
                "https://cdn.pixabay.com/photo/2015/11/12/15/58/flag-1040539_960_720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CANADÁ","2.5","OTTAWA",
                "https://economipedia.com/wp-content/uploads/Bandera-Canad%C3%A1-1.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CHILE","2.5","SANTIAGO DE CHILE",
                "https://cdn.pixabay.com/photo/2012/04/11/17/47/chile-29125_640.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("COLOMBIA","2.5","BOGOTÁ",
                "https://upload.wikimedia.org/wikipedia/commons/2/28/Bandera_Madre.PNG",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("COSTA RICA","2.5","SAN JOSÉ",
                "https://www.banderas-mundo.es/data/flags/ultra/cr.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CUBA","2.5","LA HABANA",
                "https://www.vhv.rs/dpng/d/109-1096939_bandera-de-cuba-png-transparent-png.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("DOMINICA","2.5","ROSEAU",
                "https://www.pngkey.com/png/full/841-8413982_bandera-dominicana-png-republica-dominicana-bandera-png.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("EL SALVADOR","2.5","SAN SALVADOR",
                "https://i.pinimg.com/originals/9b/14/1d/9b141df3c05035ad90900dce0da79ef6.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ESTADOS UNIDOS","2.5","WASHINGTON D. C.",
                "https://www.banderas-mundo.es/data/flags/ultra/us.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("Ecuador","2.5","quito",
                "https://upload.wikimedia.org/wikipedia/commons/8/8f/Bandera_Del_Ecuador.jpg",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GRANADA","2.5","SAINT GEORGE'S",
                "https://www.lifeder.com/wp-content/uploads/2018/12/640px-Flag_of_Grenada.svg_-1280x720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GUATEMALA","2.5","CIUDAD DE GUATEMALA",
                "https://upload.wikimedia.org/wikipedia/commons/9/9f/Bandera_de_Guatemala.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("GUYANA","2.5","GEORGETOWN",
                "https://i.pinimg.com/originals/d6/51/27/d65127f2e63eb349c4fd6092197773c3.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("HAITÍ","2.5","PUERTO PRÍNCIPE",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Flag_of_Haiti_%281849-1859%29.png/1280px-Flag_of_Haiti_%281849-1859%29.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("HONDURAS","2.5","TEGUCIGALPA",
                "https://i.pinimg.com/originals/8a/41/46/8a4146f2cf643748670ea374015fe438.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("JAMAICA","2.5","KINGSTON",
                "https://i.pinimg.com/originals/02/0b/d5/020bd5cc110a39763c77049a23b91ae9.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MÉXICO","2.5","MÉXICO D. F.",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/1200px-Flag_of_Mexico.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("NICARAGUA","2.5","MANAGUA",
                "https://cdn.pixabay.com/photo/2012/04/10/23/26/el-salvador-27001_960_720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("PANAMÁ","2.5","CIUDAD DE PANAMÁ",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Flag_of_Panama.svg/900px-Flag_of_Panama.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("PARAGUAY","2.5","ASUNCIÓN",
                "https://upload.wikimedia.org/wikipedia/commons/9/92/Bandera_de_Paraguay_%28reverso%29.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("PERÚ","2.5","LIMA",
                "https://upload.wikimedia.org/wikipedia/commons/c/c1/Bandera_Flag_Per%C3%BA_03a.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("PUERTO RICO","2.5","SAN JUAN",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Flag_of_Puerto_Rico_%281952%E2%80%931995%29.svg/1200px-Flag_of_Puerto_Rico_%281952%E2%80%931995%29.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("REPÚBLICA DOMINICANA","2.5","SANTO DOMINGO",
                "https://i.pinimg.com/originals/0c/6f/f4/0c6ff470c7c601e4915a5a673c884027.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SAN CRISTÓBAL Y NIEVES","2.5","BASSETERRE",
                "https://creazilla-store.fra1.digitaloceanspaces.com/emojis/61697/st-kitts-nevis-flag-emoji-clipart-md.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SAN VICENTE Y LAS GRANADINAS","2.5","KINGSTOWN",
                "https://upload.wikimedia.org/wikipedia/commons/7/71/Flag_of_Saint_Vincent_and_the_Grenadines_%281979%29.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SANTA LUCIA","2.5","CASTRIES",
                "https://cdn.playbuzz.com/cdn/ea7e6b27-7a6b-4223-b01f-72434a3d23b8/75bbad83-1a6e-404c-93d0-9c142b963a6f.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("SURINAM","2.5","PARAMARIBO",
                "https://i.pinimg.com/originals/2d/e3/5e/2de35ea952470f236754f3067413dcd8.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("TRINIDAD Y TOBAGO","2.5","PUERTO ESPAÑA",
                "https://i.pinimg.com/originals/ff/5b/a8/ff5ba837358c3133aff3acfdcc2d5637.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("URUGUAY","2.5","MONTEVIDEO",
                "https://i.pinimg.com/originals/84/bf/bf/84bfbfa1d564b970fa605abe435e00e4.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("VENEZUELA","2.5","CARACAS",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Flag_of_Venezuela.svg/1200px-Flag_of_Venezuela.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("AFGANISTÁN","2.5","KABUL",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Flag_of_Afghanistan_%281931%E2%80%931973%29.svg/1280px-Flag_of_Afghanistan_%281931%E2%80%931973%29.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ARABIA SAUDITA","2.5","RIAD",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Flag_of_Saudi_Arabia_%28Reverse%29.svg/1200px-Flag_of_Saudi_Arabia_%28Reverse%29.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BANGLADÉS","2.5","DACA",
                "https://www.nicepng.com/png/detail/212-2128307_flag-bangladesh-visa-bangladesh-flag-png-icons.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BARÉIN","2.5","MANAMÁ",
                "https://www.focusi.shop/uploads/country/bahrain.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BRUNEI","2.5","BANDAR SERI BEGAWAN",
                "https://www.lifeder.com/wp-content/uploads/2018/09/Fuerzas-Armadas.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("BUTÁN","2.5","TIMBU",
                "https://img.pngio.com/flag-of-bhutan-image-brief-history-of-the-flag-flag-of-bhutan-png-580_387.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CAMBOYA","2.5","PNON PEHN",
                "https://cdn.countryflags.com/thumbs/cambodia/flag-400.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CATAR","2.5","DOHA",
                "https://www.focusi.shop/uploads/country/qatar.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CHINA","2.5","PEKÍN",
                "https://economipedia.com/wp-content/uploads/Bandera-China-1.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("CHIPRE","2.5","NICOSIA",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Flag_of_Cyprus.svg/1200px-Flag_of_Cyprus.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("COREA DEL NORTE","2.5","PYONGYANG",
                "https://www.banderas-mundo.es/data/flags/w580/kp.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("EMIRATOS ARABES UNIDOS","2.5","ABU DABI",
                "https://www.lifeder.com/wp-content/uploads/2018/11/bandera-presidente-1280x720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("FILIPINAS","2.5","MANILA",
                "https://www.lifeder.com/wp-content/uploads/2018/11/640px-Flag_of_the_Philippines.svg_-1280x720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("INDIA","2.5","NUEVA DELHI",
                "https://www.banderas-mundo.es/data/flags/ultra/in.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("INDONESIA","2.5","YAKARTA",
                "https://i.pinimg.com/originals/f5/ea/57/f5ea57f8f9540fbf46d26f28d409046e.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("IRÁN","2.5","TEHERÁN",
                "https://www.banderas-mundo.es/data/flags/normal/ir.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("IRAQ","2.5","BAGDAD",
                "https://www.banderas-mundo.es/data/flags/ultra/iq.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("ISRAEL","2.5","JERUSALÉN",
                "https://i.pinimg.com/originals/b8/1e/2b/b81e2bfbab1d49632c82b91db6b102f8.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("JAPÓN","2.5","TOKIO",
                "https://i7.pngflow.com/pngimage/621/739/png-area-rectangle-font-japan-flag-miscellaneous-circle-red-area-clipart-thumb.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("KAZAJISTÁN","2.5","ASTANÁ",
                "https://comprar-banderas.com/wp-content/uploads/2017/01/bandera-de-kazajistan.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("KIRGUISTÁN","2.5","BISKEK",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Flag_of_Kyrgyzstan.svg/1200px-Flag_of_Kyrgyzstan.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("KUWAIT","2.5","CIUDAD DE KUWAIT",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Flag_of_Kuwait.svg/1200px-Flag_of_Kuwait.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("LAOS","2.5","VIENTIÁN",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Flag_of_Laos.svg/1200px-Flag_of_Laos.svg.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("LÍBANO","2.5","BEIRUT",
                "https://images.emojiterra.com/twitter/512px/1f1f1-1f1e7.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MALASIA","2.5","KUALA LUMPUR",
                "https://www.lifeder.com/wp-content/uploads/2019/05/640px-Flag_of_Malaysia.svg-1280x720.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MALDIVAS","2.5","MALÉ",
                "https://www.banderas-mundo.es/data/flags/ultra/mv.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MONGOLIA","2.5","ULAN BATOR",
                "https://simbologiadelmundo.com/wp-content/uploads/2016/06/mongolia-1.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("MYANMAR (BIRMANIA)","2.5","NAYPYIDAW",
                "https://images.emojiterra.com/openmoji/v12.2/512px/1f1f2-1f1f2.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("NEPAL","2.5","KATMANDÚ",
                "https://2.bp.blogspot.com/-6EwxCux9UBo/Wrz-FStROlI/AAAAAAAAC1w/RR7EeZTZHQAxTK1RhjdKS7r6hLASG_aeACLcBGAs/s1600/bandera-de-nepal.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("OMÁN","2.5","MASCATE",
                "https://www.banderas-mundo.es/data/flags/big/om.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        p.add(new Pais("PAKISTÁN","2.5","ISLAMABAD",
                "https://www.banderas-mundo.es/data/flags/ultra/pk.png",
                "<iframe width=\"200\" height=\"100\" src=\"https://www.youtube.com/embed/6cVX_gLZivg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));



        return p;
    }




}