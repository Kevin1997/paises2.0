package com.example.CountriesProject.adatador;
import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.CountriesProject.R;
import com.example.CountriesProject.modelView.Pais;
import java.util.List;

public class AdapterPais extends RecyclerView.Adapter<AdapterPais.MyViewHolder>{

    public List<Pais>paises;


    public static class MyViewHolder extends RecyclerView.ViewHolder{


        public TextView textPais;
        public TextView textCapital;
        public TextView textPoblacion;
        public ImageView imgPais;
        public WebView webHimno;

        public MyViewHolder(View v){
            super(v);
            textPais= v.findViewById(R.id.text_pais);
            imgPais=v.findViewById(R.id.img_pais);
            textCapital=v.findViewById(R.id.text_capital);
            textPoblacion=v.findViewById(R.id.text_poblacion);
            webHimno= v.findViewById(R.id.webHimno);

        }
    }


    public AdapterPais(List<Pais> paises){
        this.paises = paises;
    }


    @NonNull
    @Override
    public AdapterPais.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_banderas,parent, false);
        return new AdapterPais.MyViewHolder(v);
    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position){

         Pais pais=paises.get(position);
         holder.textPais.setText(pais.getNombre());
         holder.textPoblacion.setText("capital :"+pais.getPoblacion());
         holder.textCapital.setText("poblacion :"+pais.getCapital());
         Glide.with(holder.itemView.getContext()).load(pais.getUrlBandera()).into(holder.imgPais);
         holder.webHimno.getSettings().setJavaScriptEnabled(true);
         holder.webHimno.loadData(pais.getUrlHimno(),"text/html","utf-8");
         holder.webHimno.setWebChromeClient(new WebChromeClient(){});

    }

    @Override
    public int getItemCount(){
        return paises.size();
    }


}
